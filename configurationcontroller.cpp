/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "configurationcontroller.h"

const QString ConfigurationController::kKeyVolume = QString("volume");

ConfigurationController::ConfigurationController(QObject *parent)
        : QObject(parent), _settings("QtCon Brasil 2020", "HaQton quiz") {
    qRegisterMetaType<ConfigurationController *>("ConfigurationController*");
}

ConfigurationController::~ConfigurationController() {
}

float ConfigurationController::getVolume() {
    return _settings.value(kKeyVolume, 1.0).toFloat();
}

void ConfigurationController::setVolume(float volume) {
    _settings.setValue(kKeyVolume, volume);
    volumeChanged();
}
