/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

#include <QJsonObject>
#include <QString>

class Player {
 public:
    explicit Player(const QJsonObject &player_json = QJsonObject());
    ~Player();

    int getId() const;
    QString getName() const;
    QString getEmail() const;
    bool isApproved() const;
    int getScore() const;
    bool answered() const;
    int getLastAnswer() const;

    void setId(int id);
    void setName(const QString &name);
    void setEmail(const QString &email);
    void setApproval(bool approval);
    void setScore(int score);
    void setAnswered(bool answered);
    void setLastAnswer(int answer);

 private:
    int _id;
    QString _name;
    QString _email;
    bool _approved;
    int _score;
    bool _answered;
    int _last_answer;

    static const QString kKeyId;
    static const QString kKeyName;
    static const QString kKeyEmail;
    static const QString kKeyApproved;
    static const QString kKeyScore;
};

#endif  // PLAYER_H
