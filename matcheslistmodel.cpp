/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "matcheslistmodel.h"

#include <QtQml>

#include "match.h"

MatchesListModel::MatchesListModel(QObject *parent) : QAbstractListModel(parent) {
    qRegisterMetaType<MatchesListModel *>("MatchesListModel*");
    qmlRegisterInterface<MatchesListModel>("MatchesListModel", 1);
}

MatchesListModel::~MatchesListModel() {
}

int MatchesListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return _match_list.count();
}

QHash<int, QByteArray> MatchesListModel::roleNames() const {
    QHash<int, QByteArray> roleNames;
    roleNames.insert(nameRole, "name");
    roleNames.insert(playersRole, "players");
    roleNames.insert(creatorRole, "creator");
    return roleNames;
}

QVariant MatchesListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= _match_list.count()) {
        return QVariant();
    }

    QVariant value;

    if (role == nameRole) {
        value = _match_list[index.row()].getDescription();
    } else if (role == playersRole) {
        value = QString::number(_match_list[index.row()].getPlayersCount());
    } else if (role == creatorRole) {
        value = _match_list[index.row()].getCreatorName();
    }

    return value;
}

void MatchesListModel::setList(const QList<Match> &match_list) {
    beginResetModel();
    _match_list.clear();
    endResetModel();

    beginInsertRows(QModelIndex(), 0, match_list.count() - 1);
    _match_list = match_list;
    endInsertRows();
}

int MatchesListModel::getMatchId(int match_index) {
    if (match_index < 0 || match_index >= _match_list.count()) {
        return -1;
    }

    return _match_list[match_index].getId();
}

int MatchesListModel::getMatchTopic(int match_index) {
    if (match_index < 0 || match_index >= _match_list.count()) {
        return -1;
    }

    return _match_list[match_index].getTopic();
}
