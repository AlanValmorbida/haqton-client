/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef PLAYERSLISTMODEL_H
#define PLAYERSLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QString>

class Player;

class PlayersListModel : public QAbstractListModel {
    Q_OBJECT

 public:
    explicit PlayersListModel(QObject *parent = nullptr);
    ~PlayersListModel();

    enum userEventRoles {
        nameRole = Qt::UserRole + 1,
        approvedRole,
        answeredRole,
        answerRole,
        scoreRole
    };

    //! Get the number of player.
    /*!
      \param parent QModelIndex parent.
    */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    //! Get the columns roles.
    /*!
      \return The columns roles and names
    */
    QHash<int, QByteArray> roleNames() const;

    //! Get the column value of a player.
    /*!
      \param index a player index.
      \param role a column.
      \return The value
    */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    //! Set the player list.
    /*!
      \param player_list the new player list.
    */
    void setList(const QList<Player> &player_list);

    //! Set the player list.
    /*!
      \param player_list the new player list.
    */
    void onPlayerAnswered(int player_id, bool answered, int answer, bool answer_is_right);

    //! Clear the player list
    Q_INVOKABLE void clear();

    //! Start the emission of signal to notify the table update, it should be called after call
    //! stopTableUpdate
    Q_INVOKABLE void startTableUpdate();

    //! Stop the emission of signal to notify the table update, after call it, nothing signal is
    //! emitted when the table content (player list) is changed
    Q_INVOKABLE void stopTableUpdate();

    //! Get the id of a match based on its table index (match_list index).
    /*!
      \param match_index a match index.
      \return The match id
    */
    Q_INVOKABLE int getPlayerId(int player_index);

    //! Sort the player list in descending order of scores
    Q_INVOKABLE void orderByScore();

 private:
    QList<Player> _player_list;

    bool _update;
};

#endif  // PLAYERSLISTMODEL_H
