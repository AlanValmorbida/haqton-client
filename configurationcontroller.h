/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CONFIGURATIONCONTROLLER_H
#define CONFIGURATIONCONTROLLER_H

#include <QObject>
#include <QSettings>

class ConfigurationController : public QObject {
    Q_OBJECT
    Q_PROPERTY(float volume READ getVolume WRITE setVolume NOTIFY volumeChanged)

 public:
    explicit ConfigurationController(QObject *parent = nullptr);
    ~ConfigurationController();

    //! Get the configured volume.
    /*!
      \return The volume
    */
    Q_INVOKABLE float getVolume();

    //! Set the volume
    /*!
      \param volume the new volume, should be between 0.0 and 1.0.
    */
    Q_INVOKABLE void setVolume(float volume);

 Q_SIGNALS:
    /*!
      This signal is emitted when the volume has been changed (after setVolume() has been called).
      \param token new token
    */
    void volumeChanged();

 private:
    static const QString kKeyVolume;

    QSettings _settings;
};

#endif  // CONFIGURATIONCONTROLLER_H
