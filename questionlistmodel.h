/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef QUESTIONLISTMODEL_H
#define QUESTIONLISTMODEL_H

#include <QAbstractListModel>
#include <QString>

#include "question.h"

class Question;

class QuestionListModel : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY(QString description READ getQuestionDescription NOTIFY questionDescriptionChanged)

 public:
    explicit QuestionListModel(QObject *parent = nullptr);
    ~QuestionListModel();

    enum userEventRoles { optionRole = Qt::UserRole + 1, descriptionRole };

    //! Get the number of question answers.
    /*!
      \param parent QModelIndex parent.
    */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    //! Get the columns roles.
    /*!
      \return The columns roles and names
    */
    QHash<int, QByteArray> roleNames() const;

    //! Get the column value of a answer.
    /*!
      \param index a answer index.
      \param role a column.
      \return The value
    */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    //! Set the questiob.
    /*!
      \param question the new question.
    */
    void setQuestion(const Question &question);

    //! Start the emission of signal to notify the table update, it should be called after call
    //! stopTableUpdate
    Q_INVOKABLE void startTableUpdate();

    //! Stop the emission of signal to notify the table update, after call it, nothing signal is
    //! emitted when the table content is changed
    Q_INVOKABLE void stopTableUpdate();

    //! Get the question description.
    /*!
      \return The question description
    */
    Q_INVOKABLE QString getQuestionDescription();

    //! Get the id of a match based on its table index (match_list index).
    /*!
      \param match_index a match index.
      \return The match id
    */
    Q_INVOKABLE int getOptionId(int match_index);

    //! Get the right answer index for the question.
    /*!
      \return The right answer
    */
    Q_INVOKABLE int getRightOption();

 Q_SIGNALS:
    /*!
      This signal is emitted when the question has been changed (after setQuestion() has been
      called).
    */
    void questionDescriptionChanged();

 private:
    Question _question;

    bool _update;
};

#endif  // QUESTIONLISTMODEL_H
