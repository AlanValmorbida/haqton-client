/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef USERCONTROLLER_H
#define USERCONTROLLER_H

#include <QObject>
#include <QSettings>

class UserController : public QObject {
    Q_OBJECT
 public:
    explicit UserController(QObject *parent = nullptr);
    ~UserController();

    //! Check if the user configured is valid.
    /*!
      \return true if the user is valid else false
    */
    Q_INVOKABLE bool isUserValid();

    Q_INVOKABLE void setUserName(const QString &name);
    Q_INVOKABLE QString getUserName();

    Q_INVOKABLE void setUserEmail(const QString &email);
    Q_INVOKABLE QString getUserEmail();

 Q_SIGNALS:
    /*!
      This signal is emitted when the volume has been changed (after setVolume() has been called).
      \param token new token
    */
    void signalUserUpdated(const QString &name, const QString &email);

 private:
    static const QString kKeyUser;
    static const QString kKeyEmail;

    QSettings _settings;
};

#endif  // USERCONTROLLER_H
