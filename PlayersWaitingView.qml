/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 1.4 as Quick1
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Aguardando início da partida")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        Quick1.TableView {
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            frameVisible: false
            backgroundVisible: false
            selectionMode: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: Core.playersListModel
            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Participante")
                width: 3 * stackView.width / 4
                delegate: Text {
                    text: styleData.value
                    anchors.fill: parent
                    verticalAlignment: Qt.AlignVCenter
                }
            }
            Quick1.TableViewColumn {
                role: "approved"
                title: qsTr("Estado")
                width: stackView.width / 4
                delegate: Item {
                    anchors.fill: parent
                    RowLayout {
                        anchors.fill: parent
                        Label {
                            text: Icons.faHourglassHalf
                            visible: !styleData.value
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            Layout.alignment: Qt.AlignCenter
                            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 16 }
                            color: "yellow"
                            ToolTip.text: qsTr("Aguardando aprovação")
                            ToolTip.visible: maWaiting.containsMouse
                            MouseArea {
                                id: maWaiting
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                        }
                        Label {
                            text: Icons.faCheck
                            visible: styleData.value
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            Layout.alignment: Qt.AlignCenter
                            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 18 }
                            color: "green"
                            ToolTip.text: qsTr("Participação aprovada")
                            ToolTip.visible: maApproved.containsMouse
                            MouseArea {
                                id: maApproved
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                        }
                    }
                }
            }
            style: TableViewStyle {
                textColor: "black"
                alternateBackgroundColor: "#80AAAAAA"
            }
        }

        Button {
            text: qsTr("Sair")
            Layout.fillWidth: true
            onClicked: {
                Core.gameController.exitCurrentMatch()
                if (stackView) {
                    stackView.pop()
                }
            }
        }
    }

    Connections {
        id: playersWaitingViewConnections
        target: Core.gameController
        enabled: true
        function onSignalPlayerRejected() {
            if (stackView) {
                stackView.pop()
            }
        }
        function onSignalNewQuestion() {
            if (stackView) {
                stackView.push("qrc:/QuestionView.qml", {creator: false})
            }
            playersWaitingViewConnections.enabled = false
        }
    }
}
