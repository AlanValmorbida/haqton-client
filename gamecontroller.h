/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>

#include "match.h"
#include "player.h"
#include "question.h"

class RequestsController;

class GameController : public QObject {
    Q_OBJECT

 public:
    GameController(QObject *parent = nullptr);
    ~GameController();

    //! Set the player infomations.
    /*!
      \param user_name player name.
      \param user_email player email.
    */
    void setUser(const QString &user_name, const QString &user_email);

    //! Set the authentication token.
    /*!
      \param token authentication bearer token.
    */
    void setToken(const QString &token);

    //! Update the list of matches waiting for players. It is async the signal
    //! signalMatchListUpdated will be emitted when the update is finished.
    Q_INVOKABLE void updateMatchesList();

    //! Create a new match and enter in the created match.
    /*!
      \param match_name the match name.
    */
    Q_INVOKABLE void createAndEnterMatch(const QString &match_name);

    //! Delete the current match (the match entered or created)
    Q_INVOKABLE void deleteCurrentMatch();

    //! Enter in a match waiting for players.
    /*!
      \param match_id the match id.
      \param match_topic the match topic.
    */
    Q_INVOKABLE void enterMatch(int match_id, int match_topic);

    //! Exit from current match (the match entered or created).
    Q_INVOKABLE void exitCurrentMatch();

    //! Start the current match (the match entered or created), it call getRandonQuestion.
    //! automatically.
    Q_INVOKABLE void startCurrentMatch();

    //! Change the status of current match to finished.
    Q_INVOKABLE void finishCurrentMatch();

    //! Approve a player who entered the current match.
    /*!
      \param player_id the player id.
    */
    Q_INVOKABLE void approvePlayer(int player_id);

    //! Reject a player who entered the current match.
    /*!
      \param player_id the player id.
    */
    Q_INVOKABLE void rejectPlayer(int player_id);

    //! Request the next random question from the server.
    Q_INVOKABLE void getRandomQuestion();

    //! Send an answer to the server.
    /*!
      \param answer_id the answer id.
    */
    Q_INVOKABLE void processAnswer(int answer_id);

    //! Check if the device is connected to the internet.
    /*!
      \return true if connect else false
    */
    Q_INVOKABLE bool isOnline();

    //! Slot to manage a new message received on message bus.
    /*!
      \param message the new message received.
    */
    void onNewMessage(const QString &message);

 Q_SIGNALS:
    /*!
      This signal is emitted when the list of matches waiting for player is updated.
      \param match_list new token
    */
    void signalMatchListUpdated(const QList<Match> &match_list);

    /*!
      This signal is emitted when the list of match player is updated.
      \param player_list new token
    */
    void signalPlayerListUpdated(const QList<Player> &player_list);

    /*!
      This signal is emitted when a new answer is processed or to clean the previous answer.
      \param player_id the player who answered
      \param answered true to a new answer and false to clean the last answer
      \param answer the answer id
      \param answer_is_right true is the answer is right
    */
    void signalPlayerAnswered(int player_id, bool answered, int answer, bool answer_is_right);

    /*!
      This signal is emitted to change the message bus topic subscribed.
      \param topic the new topic to subscribe
    */
    void signalUpdateMessageTopic(const QString &topic);

    /*!
      This signal is emitted when a new random question is received.
      \param question the new question
    */
    void signalNewQuestion(const Question &question);

    /*!
      This signal is emitted when the current player is rejected in the entered match.
    */
    void signalPlayerRejected();

    /*!
      This signal is emitted when the player list is updated, it informs if all match players have a
      status assigned (approved or rejected).
      \param can_start true if the user can start the
      current match
    */
    void signalMatchCanBeStarted(bool can_start);

    /*!
      This signal is emitted when the current match is finished.
    */
    void signalMatchFinished();

    /*!
      This signal is emitted when a server request returns an authentication error.
    */
    void signalInvalidToken();

 private:
    void onMatchesUpdated(const QJsonDocument &matches_json);
    void onPlayersUpdated(const QJsonDocument &players_json);
    void onNewMatchCreated(const QJsonDocument &match_json);
    void onMatchEntered(const QJsonDocument &json);
    void onMatchUpdated(const QJsonDocument &json);
    void onNewQuestion(const QJsonDocument &json);
    void onNewAnswer(const QJsonDocument &json);

    void onMatchExited();

    static const QString kMessageKeyType;
    static const QString kMessageKeyData;
    static const QString kMessageKeyPlayerId;
    static const QString kMessageKeyPlayerOption;

    static const QString kMessageTypeMatchesUpdate;
    static const QString kMessageTypeMatchFinished;
    static const QString kMessageTypePlayersUpdate;
    static const QString kMessageTypeNewQuestion;
    static const QString kMessageTypeNewAnswer;

    RequestsController *_requests_controller;

    Player _user;
    Match _current_match;
    Question _current_question;
    bool _match_creator;

    bool _should_delete_match;
};

#endif  // GAMECONTROLLER_H
