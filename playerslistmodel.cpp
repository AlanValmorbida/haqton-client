/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "playerslistmodel.h"

#include <QtQml>
#include <algorithm>

#include "player.h"

PlayersListModel::PlayersListModel(QObject *parent) : QAbstractListModel(parent), _update(true) {
    qRegisterMetaType<PlayersListModel *>("PlayersListModel*");
    qmlRegisterInterface<PlayersListModel>("PlayersListModel", 1);
}

PlayersListModel::~PlayersListModel() {
}

int PlayersListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return _player_list.count();
}

QHash<int, QByteArray> PlayersListModel::roleNames() const {
    QHash<int, QByteArray> roleNames;
    roleNames.insert(nameRole, "name");
    roleNames.insert(approvedRole, "approved");
    roleNames.insert(answeredRole, "answered");
    roleNames.insert(answerRole, "answer");
    roleNames.insert(scoreRole, "score");
    return roleNames;
}

QVariant PlayersListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= _player_list.count()) {
        return QVariant();
    }

    QVariant value;

    if (role == nameRole) {
        value = _player_list[index.row()].getName();
    } else if (role == approvedRole) {
        value = _player_list[index.row()].isApproved();
    } else if (role == answeredRole) {
        value = _player_list[index.row()].answered();
    } else if (role == answerRole) {
        if (_player_list[index.row()].getLastAnswer() >= 0) {
            value = QString(QChar(_player_list[index.row()].getLastAnswer() + 65));
        } else {
            value = "-";
        }
    } else if (role == scoreRole) {
        value = _player_list[index.row()].getScore();
    }

    return value;
}

void PlayersListModel::setList(const QList<Player> &player_list) {
    if (_update) {
        beginResetModel();
        _player_list.clear();
        endResetModel();

        beginInsertRows(QModelIndex(), 0, player_list.count() - 1);
    }

    _player_list = player_list;

    if (_update) {
        endInsertRows();
    }
}

void PlayersListModel::onPlayerAnswered(int player_id, bool answered, int answer,
                                        bool answer_is_right) {
    int row = -1;

    QList<Player>::iterator player_it = std::find_if(_player_list.begin(), _player_list.end(),
                                                     [player_id, &row](const Player &player) {
                                                         row++;
                                                         return player.getId() == player_id;
                                                     });

    if (!player_it->answered() && answered && answer_is_right) {
        player_it->setScore(player_it->getScore() + 1);
    }

    player_it->setAnswered(answered);
    player_it->setLastAnswer(answer);

    if (_update) {
        dataChanged(index(row), index(row), {answeredRole, answerRole, scoreRole});
    }
}

void PlayersListModel::clear() {
    beginResetModel();
    _player_list.clear();
    endResetModel();
}

void PlayersListModel::startTableUpdate() {
    _update = true;

    QList<Player> player_list = _player_list;

    beginResetModel();
    _player_list.clear();
    endResetModel();

    beginInsertRows(QModelIndex(), 0, player_list.count() - 1);
    _player_list = player_list;
    endInsertRows();
}

void PlayersListModel::stopTableUpdate() {
    _update = false;
}

int PlayersListModel::getPlayerId(int player_index) {
    if (player_index < 0 || player_index >= _player_list.count()) {
        return -1;
    }

    return _player_list[player_index].getId();
}

void PlayersListModel::orderByScore() {
    std::sort(_player_list.begin(), _player_list.end(),
              [](const Player &player1, const Player &player2) {
                  return player1.getScore() > player2.getScore();
              });
}
