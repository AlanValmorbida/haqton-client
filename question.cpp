/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "question.h"

#include <QJsonArray>

const QString Question::kKeyId = QString("id");
const QString Question::kKeyDescription = QString("description");
const QString Question::kKeyRightOption = QString("right_option");
const QString Question::kKeyOptions = QString("question_options");

Question::Question(const QJsonObject &question_json) : _id(-1), _right_option(-1) {
    if (question_json.contains(kKeyId) && question_json[kKeyId].isDouble()) {
        _id = question_json[kKeyId].toInt();
    }
    if (question_json.contains(kKeyDescription) && question_json[kKeyDescription].isString()) {
        _description = question_json[kKeyDescription].toString();
    }
    if (question_json.contains(kKeyRightOption) && question_json[kKeyRightOption].isDouble()) {
        _right_option = question_json[kKeyRightOption].toInt();
    }
    if (question_json.contains(kKeyOptions) && question_json[kKeyOptions].isArray()) {
        for (const QJsonValue &json : question_json[kKeyOptions].toArray()) {
            if (json.isObject()) {
                QJsonObject option_json = json.toObject();
                if (option_json.contains(kKeyId) && option_json[kKeyId].isDouble() &&
                    option_json.contains(kKeyDescription) &&
                    option_json[kKeyDescription].isString()) {
                    _options.push_back(Option{option_json[kKeyId].toInt(),
                                              option_json[kKeyDescription].toString()});
                }
            }
        }
    }
}

Question::~Question() {
}

int Question::getId() const {
    return _id;
}

QString Question::getDescription() const {
    return _description;
}

int Question::getRightOption() const {
    return _right_option;
}

QList<Option> Question::getOptions() const {
    return _options;
}
