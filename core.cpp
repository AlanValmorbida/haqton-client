/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "core.h"

#include "authenticationcontroller.h"
#include "configurationcontroller.h"
#include "gamecontroller.h"
#include "matcheslistmodel.h"
#include "messagingcontroller.h"
#include "playerslistmodel.h"
#include "questionlistmodel.h"
#include "usercontroller.h"

Q_LOGGING_CATEGORY(core, "solutions.qmob.haqton.core")

Core *Core::_instance = nullptr;

Core *Core::instance() {
    if (!_instance) {
        _instance = new Core;
    }
    return _instance;
}

Core::Core(QObject *parent)
        : QObject(parent),
          _messaging_controller(new MessagingController(this)),
          _authentication_controller(new AuthenticationController(this)),
          _configuration_controller(new ConfigurationController(this)),
          _game_controller(new GameController(this)),
          _user_controller(new UserController(this)),
          _matches_list_model(new MatchesListModel(this)),
          _players_list_model(new PlayersListModel(this)),
          _question_list_model(new QuestionListModel(this)) {
    _game_controller->setUser(_user_controller->getUserName(), _user_controller->getUserEmail());
    _game_controller->setToken(_authentication_controller->getToken());

    connect(_user_controller, &UserController::signalUserUpdated, _game_controller,
            &GameController::setUser);

    connect(_authentication_controller, &AuthenticationController::signalTokenChanged,
            _game_controller, &GameController::setToken);

    connect(_messaging_controller, &MessagingController::newMessage, _game_controller,
            &GameController::onNewMessage);
    connect(_game_controller, &GameController::signalUpdateMessageTopic, _messaging_controller,
            &MessagingController::setTopic);

    connect(_game_controller, &GameController::signalMatchListUpdated, _matches_list_model,
            &MatchesListModel::setList);

    connect(_game_controller, &GameController::signalPlayerListUpdated, _players_list_model,
            &PlayersListModel::setList);
    connect(_game_controller, &GameController::signalPlayerAnswered, _players_list_model,
            &PlayersListModel::onPlayerAnswered);

    connect(_game_controller, &GameController::signalNewQuestion, _question_list_model,
            &QuestionListModel::setQuestion);
}

Core::~Core() {
    _messaging_controller->deleteLater();
    _authentication_controller->deleteLater();
    _configuration_controller->deleteLater();
    _game_controller->deleteLater();
    _user_controller->deleteLater();
    _matches_list_model->deleteLater();
    _players_list_model->deleteLater();
    _question_list_model->deleteLater();
}

MessagingController *Core::messagingController() const {
    return _messaging_controller;
}

AuthenticationController *Core::authenticationController() const {
    return _authentication_controller;
}

ConfigurationController *Core::configurationController() const {
    return _configuration_controller;
}

GameController *Core::gameController() const {
    return _game_controller;
}

UserController *Core::userController() const {
    return _user_controller;
}

MatchesListModel *Core::matchesListModel() const {
    return _matches_list_model;
}

PlayersListModel *Core::playersListModel() const {
    return _players_list_model;
}

QuestionListModel *Core::questionListModel() const {
    return _question_list_model;
}
