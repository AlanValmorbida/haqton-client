/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "authenticationcontroller.h"

#include <QCoreApplication>
#include <QDir>
#include <QFile>

const QString AuthenticationController::kKeyTokenFile = QString("token_file");

AuthenticationController::AuthenticationController(QObject *parent)
        : QObject(parent), _settings("QtCon Brasil 2020", "HaQton quiz") {
    qRegisterMetaType<AuthenticationController *>("AuthenticationController*");

    if (_settings.value(kKeyTokenFile, "").toString().isEmpty()) {
        QDir dir(QCoreApplication::applicationDirPath());
        QStringList files_list =
                dir.entryList({"*.jwt"}, QDir::Files | QDir::NoDotAndDotDot, QDir::Name);

        if (!files_list.empty()) {
            loadToken(dir.absolutePath() + "/" + files_list[0]);
            _settings.setValue(kKeyTokenFile, dir.absolutePath() + "/" + files_list[0]);
        }
    } else {
        loadToken(_settings.value(kKeyTokenFile, "").toString());
    }
}

AuthenticationController::~AuthenticationController() {
}

QString AuthenticationController::getToken() {
    return _token;
}

QString AuthenticationController::getTokenFile() {
    return _settings.value(kKeyTokenFile, "").toString();
}

void AuthenticationController::setTokenFile(const QString &token_file) {
    _settings.setValue(kKeyTokenFile, token_file);
    loadToken(token_file);
    tokenFileChanged();
}

void AuthenticationController::loadToken(const QString &token_file) {
    QFile file(token_file);

    if (!file.open(QIODevice::ReadOnly)) {
        return;
    }

    _token = QString(file.readAll()).trimmed();
    signalTokenChanged(_token);
}
