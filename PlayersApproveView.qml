/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 1.4 as Quick1
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Aguardando chegada dos jogadores")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        Quick1.TableView {
            horizontalScrollBarPolicy: 0
            frameVisible: false
            backgroundVisible: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: Core.playersListModel
            rowDelegate: Item {
                height: 36
                Rectangle {
                    anchors.fill: parent
                    color: styleData.row % 2 == 0 ? "#80AAAAAA" : "transparent"
                }
            }
            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Participante")
                width: stackView.width / 2
                delegate: Text {
                    text: styleData.value
                    anchors.fill: parent
                    verticalAlignment: Qt.AlignVCenter
                }
            }
            Quick1.TableViewColumn {
                role: "approved"
                title: qsTr("Estado")
                width: stackView.width / 2
                delegate: Component {
                    RowLayout {
                        anchors.fill: parent
                        spacing: internal.margins / 2
                        Button {
                            id: btReject
                            text: qsTr("Rejeitar")
                            enabled: styleData.row !== 0
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            bottomInset: 3
                            topInset: 3
                            onClicked: Core.gameController.rejectPlayer(Core.playersListModel.getPlayerId(styleData.row))
                        }
                        Button {
                            id: btApprove
                            text: qsTr("Aprovar")
                            enabled: !styleData.value
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            bottomInset: 3
                            topInset: 3
                            onClicked: Core.gameController.approvePlayer(Core.playersListModel.getPlayerId(styleData.row))
                        }
                    }
                }
            }
            style: TableViewStyle {
                textColor: "black"
                alternateBackgroundColor: "#AAAAAAAA"
            }
        }

        RowLayout {
            spacing: internal.margins / 2
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
            Button {
                text: qsTr("Cancelar")
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    Core.gameController.deleteCurrentMatch()
                    if (stackView) {
                        stackView.pop()
                    }
                }
            }
            Button {
                id: btStartMatch
                text: qsTr("Iniciar Partida")
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    Core.gameController.startCurrentMatch()
                }
            }
        }
    }

    Connections {
        id: playersApproveViewConnections
        target: Core.gameController
        enabled: true
        function onSignalNewQuestion() {
            if (stackView) {
                stackView.push("qrc:/QuestionView.qml", {creator: true})
            }
            playersApproveViewConnections.enabled = false
        }
        function onSignalMatchCanBeStarted(can_start) {
            btStartMatch.enabled = can_start
        }
    }
}
