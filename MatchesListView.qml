/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 1.4 as Quick1
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Lista de partidas")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        Quick1.TableView {
            id: tvMatches
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            verticalScrollBarPolicy: Qt.ScrollBarAsNeeded
            frameVisible: false
            backgroundVisible: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: Core.matchesListModel
            rowDelegate: Component {
                Button {
                    height: 35
                    bottomInset: 1
                    topInset: 1
                    background: Rectangle {
                        color: "#AAAAAA"
                        opacity: 0.6
                        radius: 2
                        width: tvMatches.width
                    }
                    onClicked: {
                        Core.playersListModel.clear()
                        Core.gameController.enterMatch(Core.matchesListModel.getMatchId(styleData.row), Core.matchesListModel.getMatchTopic(styleData.row))
                        if (stackView) {
                            stackView.push("qrc:/PlayersWaitingView.qml", {})
                        }
                    }
                }
            }

            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Nome")
                width: stackView.width / 2
            }
            Quick1.TableViewColumn {
                role: "creator"
                title: qsTr("Criador")
                width: stackView.width / 2
            }
            style: TableViewStyle {
                textColor: "black"
                alternateBackgroundColor: "transparent"
            }
        }

        Button {
            text: qsTr("Voltar")
            Layout.fillWidth: true
            onClicked: {
                if (stackView) {
                    stackView.pop()
                }
            }
        }
    }
}
