/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "questionlistmodel.h"

#include <QtQml>

QuestionListModel::QuestionListModel(QObject *parent) : QAbstractListModel(parent), _update(true) {
    qRegisterMetaType<QuestionListModel *>("QuestionListModel*");
    qmlRegisterInterface<QuestionListModel>("QuestionListModel", 1);
}

QuestionListModel::~QuestionListModel() {
}

int QuestionListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return _question.getOptions().count();
}

QHash<int, QByteArray> QuestionListModel::roleNames() const {
    QHash<int, QByteArray> roleNames;
    roleNames.insert(optionRole, "option");
    roleNames.insert(descriptionRole, "description");
    return roleNames;
}

QVariant QuestionListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= _question.getOptions().count()) {
        return QVariant();
    }

    QVariant value;

    if (role == optionRole) {
        value = QString(QChar(index.row() + 65));
    } else if (role == descriptionRole) {
        value = _question.getOptions()[index.row()].description;
    }

    return value;
}

void QuestionListModel::setQuestion(const Question &question) {
    if (_update) {
        beginResetModel();
        _question = Question();
        endResetModel();

        beginInsertRows(QModelIndex(), 0, question.getOptions().count() - 1);
    }
    _question = question;

    if (_update) {
        endInsertRows();
        questionDescriptionChanged();
    }
}

void QuestionListModel::startTableUpdate() {
    _update = true;

    beginRemoveRows(QModelIndex(), 0, _question.getOptions().count() - 1);
    endRemoveRows();

    beginInsertRows(QModelIndex(), 0, _question.getOptions().count() - 1);
    endInsertRows();

    questionDescriptionChanged();
}

void QuestionListModel::stopTableUpdate() {
    _update = false;
}

QString QuestionListModel::getQuestionDescription() {
    return _question.getDescription();
}

int QuestionListModel::getOptionId(int option_index) {
    if (option_index < 0 || option_index >= _question.getOptions().count()) {
        return -1;
    }

    return _question.getOptions()[option_index].id;
}

int QuestionListModel::getRightOption() {
    return _question.getRightOption();
}
