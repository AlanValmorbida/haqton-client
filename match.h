/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef MATCH_H
#define MATCH_H

#include <QJsonObject>
#include <QList>
#include <QString>

#include "player.h"

enum class MatchStatus : uint8_t { Waiting = 0, Running = 1, Finished = 2 };

class Match {
 public:
    explicit Match(const QJsonObject &match_json = QJsonObject());
    ~Match();

    //! Get the Match as json.
    /*!
      \return The match json
    */
    QJsonObject toJson();

    int getId() const;
    QString getDescription() const;
    QString getCreatorName() const;
    QString getCreatorEmail() const;
    MatchStatus getStatus() const;
    int getTopic() const;
    int getPlayersCount() const;
    const QList<Player> &getPlayerList() const;

    void setId(int id);
    void setStatus(MatchStatus status);
    void setTopic(int topic);
    void setPlayerList(const QList<Player> &player_list);

 private:
    int _id;
    QString _name;
    QString _creator_name;
    QString _creator_email;
    MatchStatus _status;
    int _topic;
    QList<Player> _player_list;

    static const QString kKeyId;
    static const QString kKeyDescription;
    static const QString kKeyCreatorName;
    static const QString kKeyCreatorEmail;
    static const QString kKeyStatus;
    static const QString kKeyTopic;
    static const QString kKeyPlayers;
};

#endif  // MATCH_H
