/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 1.4 as Quick1
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Resultado final")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        Quick1.TableView {
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            frameVisible: false
            backgroundVisible: false
            selectionMode: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: Core.playersListModel
            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Colocação")
                width: stackView.width / 4
                delegate: Text {
                    text: styleData.row < 3 ? (styleData.row + 1) + "º" + " " + Icons.faTrophy : (styleData.row + 1) + "º"
                    font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 16; bold: styleData.row < 3}
                    style: styleData.row < 3 ? Text.Outline : Text.Normal
                    styleColor: "black"
                    anchors.fill: parent
                    verticalAlignment: Qt.AlignVCenter
                    color: getColorPosition(styleData.row + 1)
                    function getColorPosition(position) {
                        if (position === 1) {
                            return "#D6AF36"
                        } else if (position === 2) {
                            return "#A7A7AD"
                        } else if (position === 3) {
                            return "#A77044"
                        } else {
                            return "black"
                        }
                    }
                }
            }
            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Participante")
                width: stackView.width / 2
                delegate: Text {
                    text: styleData.value
                    anchors.fill: parent
                    verticalAlignment: Qt.AlignVCenter
                }
            }
            Quick1.TableViewColumn {
                role: "score"
                title: qsTr("Pontuação")
                width: stackView.width / 4
                delegate: Text {
                    text: styleData.value
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignRight
                    verticalAlignment: Qt.AlignVCenter
                }
            }
            style: TableViewStyle {
                textColor: "black"
                alternateBackgroundColor: "#AAAAAAAA"
            }
        }

        Button {
            text: qsTr("Retornar")
            Layout.fillWidth: true
            onClicked: {
                Core.gameController.exitCurrentMatch()
                if (stackView) {
                    stackView.pop(null)
                }
            }
        }
    }

    Component.onCompleted: Core.playersListModel.orderByScore()
}
