/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "usercontroller.h"

const QString UserController::kKeyUser = QString("user");
const QString UserController::kKeyEmail = QString("email");

UserController::UserController(QObject *parent)
        : QObject(parent), _settings("QtCon Brasil 2020", "HaQton quiz") {
    qRegisterMetaType<UserController *>("UserController*");
}

UserController::~UserController() {
}

bool UserController::isUserValid() {
    return !_settings.value(kKeyUser, "").toString().isEmpty() ||
           !_settings.value(kKeyEmail, "").toString().isEmpty();
}

void UserController::setUserName(const QString &name) {
    _settings.setValue(kKeyUser, name);
    signalUserUpdated(_settings.value(kKeyUser, "").toString(),
                      _settings.value(kKeyEmail, "").toString());
}

QString UserController::getUserName() {
    return _settings.value(kKeyUser, "").toString();
}

void UserController::setUserEmail(const QString &email) {
    _settings.setValue(kKeyEmail, email);
    signalUserUpdated(_settings.value(kKeyUser, "").toString(),
                      _settings.value(kKeyEmail, "").toString());
}

QString UserController::getUserEmail() {
    return _settings.value(kKeyEmail, "").toString();
}
