/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef AUTHENTICATIONCONTROLLER_H
#define AUTHENTICATIONCONTROLLER_H

#include <QObject>
#include <QSettings>

class AuthenticationController : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString tokenFile READ getTokenFile WRITE setTokenFile NOTIFY tokenFileChanged)

 public:
    explicit AuthenticationController(QObject *parent = nullptr);
    ~AuthenticationController();

    //! Get the bearer token required to authenticate requests.
    /*!
      \return The token
    */
    QString getToken();

    //! Get the token file (jwt file).
    /*!
      \return The file path
    */
    Q_INVOKABLE QString getTokenFile();

    //! Set the token file.
    /*!
      \param token_file The new token file path.
    */
    Q_INVOKABLE void setTokenFile(const QString &token_file);

 Q_SIGNALS:
    /*!
      This signal is emitted when the token file has been changed (after setTokenFile() has been
      called).
    */
    void tokenFileChanged();

    /*!
      This signal is emitted when the token has been changed (after setTokenFile() has been called).
      \param token new token
    */
    void signalTokenChanged(const QString &token);

 private:
    void loadToken(const QString &token_file);

    static const QString kKeyTokenFile;

    QSettings _settings;
    QString _token;
};

#endif  // AUTHENTICATIONCONTROLLER_H
