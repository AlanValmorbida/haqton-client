/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15

import "FontAwesome"

Button {
    id: button

    property alias iconLabel: iconLabel

    contentItem: Item {
        implicitHeight: iconLabel.implicitHeight + textLabel.implicitHeight
        ColumnLayout {
            anchors.centerIn: parent
            width: parent.width; height: parent.height
            Label {
                id: iconLabel
                Layout.alignment: Qt.AlignHCenter
                font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 64 }
            }
            Label {
                id: textLabel
                text: button.text
                font: button.font
                Layout.fillWidth: true; Layout.fillHeight: true
                horizontalAlignment: Text.AlignHCenter
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
            }
        }
    }

    topInset: 0; bottomInset: 0
    font.capitalization: Font.AllUppercase

    Material.background: "white"
}
