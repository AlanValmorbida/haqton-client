/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef QUESTION_H
#define QUESTION_H

#include <QJsonObject>
#include <QMap>
#include <QString>

struct Option {
    int id;
    QString description;
};

class Question {
 public:
    explicit Question(const QJsonObject &question_json = QJsonObject());
    ~Question();

    int getId() const;
    QString getDescription() const;
    int getRightOption() const;
    QList<Option> getOptions() const;

 private:
    static const QString kKeyId;
    static const QString kKeyDescription;
    static const QString kKeyRightOption;
    static const QString kKeyOptions;

    int _id;
    QString _description;
    int _right_option;
    QList<Option> _options;
};

#endif  // QUESTION_H
