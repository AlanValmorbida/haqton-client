/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    id: aboutHaqtonView
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Sobre o HaQton")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        ScrollView {
            id: scrollView
            Layout.fillHeight: true
            Layout.fillWidth: true
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            clip: true
            Label {
                text: qsTr("O HaQton tem como objetivo promover o uso do Qt na comunidade Brasileira e " +
                           "Latino-Americana através de uma competição de programação. Nesta competição, " +
                           "as equipes inscritas irão desenvolver uma aplicação Qt que requer o uso de " +
                           "funcionalidades relacionadas a UIs, multimídia, comunicação em rede via APIs " +
                           "RESTful e comunicação via barramentos de mensagens. Com isso, esperamos que " +
                           "novos talentos passem a integrar o ecossistema Qt brasileiro e latino-americano, " +
                           "através de discussões no canal Qt Brasil no Telegram, participação como prováveis " +
                           "palestrantes em edições futuras da QtCon Brasil, colaborações em projetos " +
                           "tais como o KDE, etc.<br>Saiba mais: ") +
                           "<a href='https://br.qtcon.org/'>https://br.qtcon.org/</a>"
                font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                horizontalAlignment: Text.AlignHCenter
                width: aboutHaqtonView.width
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }

        Button {
            anchors.margins: 20
            text: qsTr("Voltar")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
            onClicked: if (stackView) stackView.pop()
        }

    }
}
