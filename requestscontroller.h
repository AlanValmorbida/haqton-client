/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef REQUESTSCONTROLLER_H
#define REQUESTSCONTROLLER_H

#include <QNetworkAccessManager>
#include <QObject>

/*!
  Class to manage requests, all methods are async.
*/
class RequestsController : public QObject {
    Q_OBJECT
 public:
    explicit RequestsController(const QString &token, QObject *parent = nullptr);
    ~RequestsController();

    //! Set the authentication bearer token.
    /*!
      \param token the bearer token.
    */
    void setToken(const QString &token);

    //! Update the list of matches waiting for players. It is async and the signal signalMatchListUpdated will be emitted when the update is finished.
    void getMatches();

    //! Create a new match. It is async and the signal signalNewMatchCreated will be emitted when the creation is finished.
    /*!
      \param match_name the match name.
      \param user_name the creator name.
      \param user_email the creator email.
    */
    void createMatch(const QString &match_name, const QString &user_name,
                     const QString &user_email);

    //! Delete a existent match.
    /*!
      \param match_id the match id.
    */
    void deleteMatch(int match_id);

    //! Enter in an existent match. It is async and the signal signalMatchEntered will be emitted when the player is in the match.
    /*!
      \param match_id the match id.
      \param user_name the player name.
      \param user_email the player email.
    */
    void enterMatch(int match_id, const QString &user_name, const QString &user_email);

    //! Update the status of a match. It is async and the signal signalMatchUpdated will be emitted when the update is finished.
    /*!
      \param match_id the match id.
      \param status the new status.
    */
    void updateMatchStatus(int match_id, int status);

    //! Update the player status in a match. It is async and the signal signalPlayersUpdated will be emitted when the update is finished.
    /*!
      \param match_id the match id.
      \param player_id the player id.
      \param approved true to approve or false to reject the participation in the match.
    */
    void updatePlayer(int match_id, int player_id, bool approved);

    //! Request the next random question from the server.
    /*!
      \param match_id the match id.
    */
    void getRandomQuestion(int match_id);

    //! Send an answer for a match question
    /*!
      \param match_id the match id.
      \param player_id the player id.
      \param question_id the question id.
      \param answer_id the answer id.
    */
    void processAnswer(int match_id, int player_id, int question_id, int answer_id);

 Q_SIGNALS:
    /*!
      This signal is emitted when the match list is updated (after getMatches() has been called).
      \param matches_json the match list as json
    */
    void signalMatchListUpdated(const QJsonDocument &matches_json);

    /*!
      This signal is emitted when a new match is created (after createMatch() has been called).
      \param match_json the match as json
    */
    void signalNewMatchCreated(const QJsonDocument &match_json);

    /*!
      This signal is emitted when the match status is updated (after enterMatch() has been called).
      \param match_json the match as json
    */
    void signalMatchEntered(const QJsonDocument &match_json);

    /*!
      This signal is emitted when the match is updated (after updateMatchStatus() has been called).
      \param match_json the match as json
    */
    void signalMatchUpdated(const QJsonDocument &match_json);

    /*!
      This signal is emitted when the player status is updated (after updatePlayer() has been called).
      \param player_json the player as json
    */
    void signalPlayersUpdated(const QJsonDocument &player_json);

    /*!
      This signal is emitted when the server reports that the bearer token is invalid.
    */
    void signalInvalidToken();

 private:
    QNetworkRequest createRequest(const QString &url);
    bool checkErrors(const QJsonDocument &json);

    static const QString kUrlMatches;
    static const QString kKeyError;
    static const QString kErrorTokenInvalid;

    QNetworkAccessManager _network_manager;
    QString _token;
};

#endif  // REQUESTSCONTROLLER_H
