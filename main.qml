/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import Qt.labs.platform 1.1

import solutions.qmob.haqton 1.0

import "FontAwesome"

ApplicationWindow {
    id: applicationWindow
    width: visibility == Qt.WindowFullScreen ? 0 : 380
    height: visibility == Qt.WindowFullScreen ? 0 : width * 16/9
    visible: true
    title: qsTr("HaQton")
    visibility: "AutomaticVisibility"

    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }

    Audio {
        source: "assets/audio/Magic-Clock-Shop_Looping.mp3"
        loops: Audio.Infinite
        autoPlay: true
        volume: Core.configurationController.volume
    }

    QtObject {
        id: internal
        property int margins: 10
    }
    
    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }
    }

    MessageDialog {
        id: messageDialogConnection
        buttons: MessageDialog.Ok
        text: qsTr("Verifique sua conexão com a intenet e tente novamente.")
        title: qsTr("Erro de conexão")
    }

    property var functionMap: {
        "Criar uma partida": function openNewmatch() {
            if (Core.gameController.isOnline()) {
                stackView.push("qrc:/NewMatchView.qml", {})
            } else {
                messageDialogConnection.open()
            }
        },
        "Participar de uma partida": function openMatchesList() {
            if (Core.gameController.isOnline()) {
                Core.gameController.updateMatchesList()
                stackView.push("qrc:/MatchesListView.qml", {})
            } else {
                messageDialogConnection.open()
            }
        },
        "Sobre o HaQton": function openAboutView() {
            stackView.push("qrc:/AboutHaqtonView.qml", {})
        },
        "Patrocinadores": function openSponsorsView() {
            stackView.push("qrc:/AboutSponsorsView.qml", {})
        },
        "Configurações": function openConfigView() {
            stackView.push("qrc:/ConfigurationView.qml", {})
        }
    }

    StackView {
        id: stackView
        anchors { fill: parent; margins: internal.margins }
        initialItem: Item {
            ColumnLayout {
                width: parent.width
                anchors.centerIn: parent
                Label {
                    id: haqtonLabel
                    font { family: gameFont.name; pixelSize: 64 }
                    Layout.alignment: Qt.AlignHCenter
                    color: "white"; text: "HaQton!"
                    SequentialAnimation on scale {
                        loops: Animation.Infinite
                        PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
                        PauseAnimation { duration: 10000 }
                    }
                    SequentialAnimation on rotation {
                        loops: Animation.Infinite
                        PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
                        PauseAnimation { duration: 10000 }
                    }
                }
                GridLayout {
                    columns: stackView.height > (0.7 * stackView.width) ? 2 : 3
                    columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
                    Layout.fillWidth: true; Layout.fillHeight: false
                    Repeater {
                        model: [
                            {
                                icon: Icons.faPlusCircle,
                                text: qsTr("Criar uma partida")
                            },
                            {
                                icon: Icons.faGamepad,
                                text: qsTr("Participar de uma partida")
                            },
                            {
                                icon: Icons.faDiceD20,
                                text: qsTr("Sobre o HaQton")
                            },
                            {
                                icon: Icons.faMedal,
                                text: qsTr("Patrocinadores")
                            },
                            {
                                icon: Icons.faTools,
                                text: qsTr("Configurações")
                            }
                        ]
                        GameButton {
                            Layout.fillWidth: true; Layout.fillHeight: true
                            Layout.columnSpan: modelData.text === qsTr("Configurações") ? 2 : 1
                            Layout.alignment: Qt.AlignCenter
                            iconLabel { text: modelData.icon; color: mainRect.color }
                            text: modelData.text
                            onClicked: {
                                functionMap[modelData.text]()
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        if (!Core.userController.isUserValid()) {
            stackView.push("qrc:/ConfigurationView.qml", {initialTab: 0})
        } else {
            Core.gameController.updateMatchesList()
        }
    }

    MessageDialog {
        id: messageDialogInvalidToken
        buttons: MessageDialog.Ok
        text: qsTr("Por favor, selecione um token válido em configurações.")
        title: qsTr("Erro de autenticação")
        onAccepted: {
            stackView.pop(null)
            stackView.push("qrc:/ConfigurationView.qml", {initialTab: 1})
        }
    }

    Connections {
        target: Core.gameController
        function onSignalInvalidToken() {
            messageDialogInvalidToken.open()
        }
    }
}
