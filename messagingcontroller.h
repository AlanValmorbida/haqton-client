/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef MESSAGINGCONTROLLER_H_
#define MESSAGINGCONTROLLER_H_

#include <QLoggingCategory>
#include <QObject>

#include "zeromqsubscriberthread.h"

class MessagingController : public QObject {
    Q_OBJECT

 public:
    MessagingController(QObject *parent = nullptr);
    ~MessagingController() Q_DECL_OVERRIDE = default;

    //! Set the topic to subscribe.
    /*!
      \param topic the new topic.
    */
    Q_INVOKABLE void setTopic(QString topic);

 Q_SIGNALS:
    /*!
      This signal is emitted when a new message is received on the message bus.
      \param message the new message received
    */
    void newMessage(const QString &message);

 private:
    QString _ZMQSERVERURL = QStringLiteral("tcp://zmq.qmob.solutions");

    void connectToZeroMQProxy();

    ZeroMQSubscriberThread _zeroMQSubscriberThread;
};

#endif  // MESSAGINGCONTROLLER_H_
