/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "gamecontroller.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QtQml>

#include "player.h"
#include "question.h"
#include "requestscontroller.h"

const QString GameController::kMessageKeyType = QString("message");
const QString GameController::kMessageKeyData = QString("data");
const QString GameController::kMessageKeyPlayerId = QString("player_id");
const QString GameController::kMessageKeyPlayerOption = QString("player_option");

const QString GameController::kMessageTypeMatchesUpdate = QString("matches_update");
const QString GameController::kMessageTypeMatchFinished = QString("match_finished");
const QString GameController::kMessageTypePlayersUpdate = QString("players_update");
const QString GameController::kMessageTypeNewQuestion = QString("new_question");
const QString GameController::kMessageTypeNewAnswer = QString("new_answer");

GameController::GameController(QObject *parent)
        : QObject(parent),
          _requests_controller(new RequestsController("", this)),
          _match_creator(false),
          _should_delete_match(false) {
    qmlRegisterInterface<GameController>("GameController", 1);

    _user.setId(-1);

    connect(_requests_controller, &RequestsController::signalMatchListUpdated, this,
            &GameController::onMatchesUpdated);
    connect(_requests_controller, &RequestsController::signalNewMatchCreated, this,
            &GameController::onNewMatchCreated);
    connect(_requests_controller, &RequestsController::signalMatchEntered, this,
            &GameController::onMatchEntered);
    connect(_requests_controller, &RequestsController::signalMatchUpdated, this,
            &GameController::onMatchUpdated);

    connect(_requests_controller, &RequestsController::signalInvalidToken, this,
            &GameController::signalInvalidToken);
}

GameController::~GameController() {
    _requests_controller->deleteLater();
}

void GameController::setUser(const QString &user_name, const QString &user_email) {
    _user.setName(user_name);
    _user.setEmail(user_email);
}

void GameController::setToken(const QString &token) {
    _requests_controller->setToken(token);
}

void GameController::updateMatchesList() {
    _requests_controller->getMatches();
}

void GameController::createAndEnterMatch(const QString &match_name) {
    _match_creator = true;
    _requests_controller->createMatch(match_name, _user.getName(), _user.getEmail());
}

void GameController::deleteCurrentMatch() {
    if (_current_match.getId() == -1) {
        return;
    }

    if (_current_match.getPlayerList().count() > 1) {
        _should_delete_match = true;

        // it is necessary because the RESTful back-end does not send a players_update messege if it
        // has received multiple update player requests at the same time
        bool request_sent = false;

        for (int i = 0; i < _current_match.getPlayersCount() && !request_sent; i++) {
            if (_current_match.getPlayerList()[i].getId() != _user.getId()) {
                request_sent = true;
                _requests_controller->updatePlayer(
                        _current_match.getId(), _current_match.getPlayerList()[i].getId(), false);
            }
        }
    } else {
        _should_delete_match = false;
        _requests_controller->deleteMatch(_current_match.getId());
        onMatchExited();
    }
}

void GameController::enterMatch(int match_id, int match_topic) {
    _match_creator = false;
    _current_match.setId(match_id);
    _current_match.setTopic(match_topic);
    _requests_controller->enterMatch(_current_match.getId(), _user.getName(), _user.getEmail());
}

void GameController::exitCurrentMatch() {
    _requests_controller->updatePlayer(_current_match.getId(), _user.getId(), false);
    onMatchExited();
}

void GameController::startCurrentMatch() {
    if (_current_match.getId() == -1) {
        return;
    }

    _requests_controller->updateMatchStatus(_current_match.getId(),
                                            static_cast<int>(MatchStatus::Running));
}

void GameController::finishCurrentMatch() {
    _requests_controller->updateMatchStatus(_current_match.getId(),
                                            static_cast<int>(MatchStatus::Finished));
}

void GameController::approvePlayer(int player_id) {
    _requests_controller->updatePlayer(_current_match.getId(), player_id, true);
}

void GameController::rejectPlayer(int player_id) {
    _requests_controller->updatePlayer(_current_match.getId(), player_id, false);
}

void GameController::getRandomQuestion() {
    _requests_controller->getRandomQuestion(_current_match.getId());
}

void GameController::processAnswer(int answer_id) {
    _requests_controller->processAnswer(_current_match.getId(), _user.getId(),
                                        _current_question.getId(), answer_id);
}

bool GameController::isOnline() {
    return QNetworkConfigurationManager(this).isOnline();
}

void GameController::onNewMessage(const QString &message) {
    QJsonDocument json = QJsonDocument::fromJson(message.toUtf8());

    if (!json.isObject()) {
        return;
    }

    QJsonObject json_obj = json.object();

    if (!json_obj.contains(kMessageKeyType) || !json_obj[kMessageKeyType].isString()) {
        return;
    }

    QString message_type = json_obj[kMessageKeyType].toString();

    if (message_type == kMessageTypeMatchFinished) {
        onMatchExited();
        signalMatchFinished();
        return;
    }

    if (!json_obj.contains(kMessageKeyData)) {
        return;
    }

    if (message_type == kMessageTypeMatchesUpdate) {
        if (json_obj[kMessageKeyData].isArray()) {
            onMatchesUpdated(QJsonDocument(json_obj[kMessageKeyData].toArray()));
        }
    } else if (message_type == kMessageTypePlayersUpdate) {
        if (json_obj[kMessageKeyData].isArray()) {
            onPlayersUpdated(QJsonDocument(json_obj[kMessageKeyData].toArray()));
        }
    } else if (message_type == kMessageTypeNewQuestion) {
        if (json_obj[kMessageKeyData].isObject()) {
            onNewQuestion(QJsonDocument(json_obj[kMessageKeyData].toObject()));
        }
    } else if (message_type == kMessageTypeNewAnswer) {
        if (json_obj[kMessageKeyData].isObject()) {
            onNewAnswer(QJsonDocument(json_obj[kMessageKeyData].toObject()));
        }
    }
}

void GameController::onMatchesUpdated(const QJsonDocument &matches_json) {
    if (matches_json.isArray()) {
        QList<Match> match_list;
        for (const QJsonValue &match_json : matches_json.array()) {
            if (match_json.isObject()) {
                match_list.push_back(Match(match_json.toObject()));
            }
        }
        signalMatchListUpdated(match_list);
    }
}

void GameController::onPlayersUpdated(const QJsonDocument &players_json) {
    if (players_json.isArray()) {
        QList<Player> player_list;
        bool player_rejected = true;
        bool all_players_approved = true;
        for (const QJsonValue &player_json : players_json.array()) {
            if (player_json.isObject()) {
                Player player(player_json.toObject());
                for (QList<Player>::const_iterator player_it =
                             _current_match.getPlayerList().begin();
                     player_it != _current_match.getPlayerList().end(); ++player_it) {
                    if (player_it->getId() == player.getId()) {
                        player.setAnswered(player_it->answered());
                        player.setLastAnswer(player_it->getLastAnswer());
                    }
                }
                player_list.push_back(player);
                player_rejected &= (player_list.last().getId() != _user.getId());
                all_players_approved &= player_list.last().isApproved();
            }
        }
        _current_match.setPlayerList(player_list);
        signalPlayerListUpdated(player_list);
        signalMatchCanBeStarted(all_players_approved);

        if (player_rejected && _user.getId() != -1) {
            onMatchExited();
            signalPlayerRejected();
        }

        if (_should_delete_match) {
            deleteCurrentMatch();
        }

        if (_match_creator && _current_match.getStatus() == MatchStatus::Running) {
            bool all_players_answered = true;

            for (QList<Player>::const_iterator player_it = _current_match.getPlayerList().begin();
                 player_it != _current_match.getPlayerList().end(); ++player_it) {
                all_players_answered &= player_it->answered();
            }

            if (all_players_answered) {
                getRandomQuestion();
            }
        }
    }
}

void GameController::onNewMatchCreated(const QJsonDocument &match_json) {
    if (match_json.isObject()) {
        _current_match = Match(match_json.object());
        _user.setId(_current_match.getPlayerList().first().getId());
        signalUpdateMessageTopic(QString::number(_current_match.getTopic()));
        signalPlayerListUpdated(_current_match.getPlayerList());
    }
}

void GameController::onMatchEntered(const QJsonDocument &json) {
    if (json.isArray()) {
        QList<Player> player_list;
        for (const QJsonValue &player_json : json.array()) {
            if (player_json.isObject()) {
                player_list.push_back(Player(player_json.toObject()));
            }
        }
        _user.setId(player_list.last().getId());
        _current_match.setPlayerList(player_list);
        signalPlayerListUpdated(player_list);
        signalUpdateMessageTopic(QString::number(_current_match.getTopic()));
    }
}

void GameController::onMatchUpdated(const QJsonDocument &match_json) {
    if (match_json.isObject()) {
        Match match(match_json.object());

        if (_current_match.getStatus() == MatchStatus::Waiting &&
            match.getStatus() == MatchStatus::Running) {
            getRandomQuestion();
        }

        _current_match = match;
    }
}

void GameController::onNewQuestion(const QJsonDocument &json) {
    if (json.isObject()) {
        _current_question = Question(json.object());
        signalNewQuestion(_current_question);

        for (QList<Player>::const_iterator player_it = _current_match.getPlayerList().begin();
             player_it != _current_match.getPlayerList().end(); ++player_it) {
            const_cast<Player &>(*player_it).setAnswered(false);
            signalPlayerAnswered(player_it->getId(), false, -1, false);
        }
    }
}

void GameController::onNewAnswer(const QJsonDocument &json) {
    if (json.isObject()) {
        QJsonObject json_object = json.object();

        if (json_object.contains(kMessageKeyPlayerId) &&
            json_object[kMessageKeyPlayerId].isDouble() &&
            json_object.contains(kMessageKeyPlayerOption) &&
            json_object[kMessageKeyPlayerOption].isDouble()) {
            int player_id = json_object[kMessageKeyPlayerId].toInt();
            int answer = json_object[kMessageKeyPlayerOption].toInt();

            signalPlayerAnswered(player_id, true, answer,
                                 answer == _current_question.getRightOption());

            bool all_players_answered = true;

            for (QList<Player>::const_iterator player_it = _current_match.getPlayerList().begin();
                 player_it != _current_match.getPlayerList().end(); ++player_it) {
                if (player_it->getId() == player_id) {
                    const_cast<Player &>(*player_it).setAnswered(true);
                    const_cast<Player &>(*player_it).setLastAnswer(answer);
                }

                all_players_answered &= player_it->answered();
            }

            if (_match_creator && all_players_answered) {
                getRandomQuestion();
            }
        }
    }
}

void GameController::onMatchExited() {
    signalUpdateMessageTopic("matches");
    _current_match = Match();
    updateMatchesList();
}
