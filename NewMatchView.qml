/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Criar nova partida")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        RowLayout {
            spacing: internal.margins
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Item{
                Layout.fillWidth: true
            }
            Label {
                id: lblMatchDescription
                text: qsTr("Nome:")
                font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                horizontalAlignment: Text.AlignRight
                color: "#1b1b1b"
                wrapMode: Text.WordWrap
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            TextField {
                id: tfMatchDescription
                text: ""
                font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 18 }
                selectByMouse: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.maximumWidth: 300
            }
            Item{
                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: internal.margins / 2
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
            Button {
                text: qsTr("Voltar")
                Layout.fillWidth: true
                onClicked: if (stackView) stackView.pop()
            }
            Button {
                text: qsTr("Criar")
                Layout.fillWidth: true
                onClicked: {
                    if (tfMatchDescription.text.length === 0) {
                        var component = Qt.createComponent("MessageDialog.qml")
                        if (component.status === Component.Ready) {
                            component.createObject(parent, {text: "teste"});
                        }
                    } else {
                        Core.playersListModel.clear()
                        Core.gameController.createAndEnterMatch(tfMatchDescription.text)
                        if (stackView) {
                            stackView.push("qrc:/PlayersApproveView.qml", {})
                        }
                    }
                }
            }
        }
    }
}
