/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef MATCHESLISTMODEL_H
#define MATCHESLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QString>

class Match;

class MatchesListModel : public QAbstractListModel {
    Q_OBJECT

 public:
    explicit MatchesListModel(QObject *parent = nullptr);
    ~MatchesListModel();

    enum userEventRoles { nameRole = Qt::UserRole + 1, playersRole, creatorRole };

    //! Get the number of matches.
    /*!
      \param parent QModelIndex parent.
    */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    //! Get the columns roles.
    /*!
      \return The columns roles and names
    */
    QHash<int, QByteArray> roleNames() const;

    //! Get the column value of a match.
    /*!
      \param index a match index.
      \param role a column.
      \return The value
    */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    //! Set the match list.
    /*!
      \param match_list the new match list.
    */
    void setList(const QList<Match> &match_list);

    //! Get the id of a match based on its table index (match_list index).
    /*!
      \param match_index a match index.
      \return The match id
    */
    Q_INVOKABLE int getMatchId(int match_index);

    //! Get the topic of a match based on its table index (match_list index).
    /*!
      \param match_index a match index.
      \return The match topic
    */
    Q_INVOKABLE int getMatchTopic(int match_index);

 private:
    QList<Match> _match_list;
};

#endif  // MATCHESLISTMODEL_H
