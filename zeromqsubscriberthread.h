/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef ZEROMQSUBSCRIBERTHREAD_H
#define ZEROMQSUBSCRIBERTHREAD_H

#include <QThread>

#include "zhelpers.h"

class ZeroMQSubscriberThread : public QThread
{
    Q_OBJECT

public:
    ZeroMQSubscriberThread(QString url, QObject *parent = nullptr);

    //! The thread process.
    void run() Q_DECL_OVERRIDE;

    //! Set the topic to subscribe.
    /*!
      \param topic the new topic.
    */
    void setTopic(QString topic);

Q_SIGNALS:
    /*!
      This signal is emitted when a new message is received.
      \param message the new message received
    */
    void newMessage(const QString &message);

private:
    QString _url;
    QString _topic;
    zmq::context_t _context;
    zmq::socket_t _subscriber;
};

#endif // ZEROMQSUBSCRIBERTHREAD_H
