/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "match.h"

#include <QJsonArray>

const QString Match::kKeyId = QString("id");
const QString Match::kKeyDescription = QString("description");
const QString Match::kKeyCreatorName = QString("creator_name");
const QString Match::kKeyCreatorEmail = QString("creator_email");
const QString Match::kKeyStatus = QString("status");
const QString Match::kKeyTopic = QString("topic");
const QString Match::kKeyPlayers = QString("match_players");

Match::Match(const QJsonObject &match_json) : _id(-1), _status(MatchStatus::Waiting), _topic(-1) {
    if (match_json.contains(kKeyId) && match_json[kKeyId].isDouble()) {
        _id = match_json[kKeyId].toInt();
    }
    if (match_json.contains(kKeyDescription) && match_json[kKeyDescription].isString()) {
        _name = match_json[kKeyDescription].toString();
    }
    if (match_json.contains(kKeyCreatorName) && match_json[kKeyCreatorName].isString()) {
        _creator_name = match_json[kKeyCreatorName].toString();
    }
    if (match_json.contains(kKeyCreatorEmail) && match_json[kKeyCreatorEmail].isString()) {
        _creator_email = match_json[kKeyCreatorEmail].toString();
    }
    if (match_json.contains(kKeyStatus) && match_json[kKeyStatus].isDouble()) {
        _status = MatchStatus(match_json[kKeyStatus].toInt());
    }
    if (match_json.contains(kKeyTopic) && match_json[kKeyTopic].isDouble()) {
        _topic = match_json[kKeyTopic].toInt();
    }
    if (match_json.contains(kKeyPlayers) && match_json[kKeyPlayers].isArray()) {
        for (const QJsonValue &json : match_json[kKeyPlayers].toArray()) {
            if (json.isObject()) {
                _player_list.push_back(Player(json.toObject()));
            }
        }
    }
}

Match::~Match() {
}

QJsonObject Match::toJson() {
    QJsonObject json;

    json.insert(kKeyDescription, _name);
    json.insert(kKeyCreatorName, _creator_name);
    json.insert(kKeyCreatorEmail, _creator_email);

    return json;
}

int Match::getId() const {
    return _id;
}

QString Match::getDescription() const {
    return _name;
}

QString Match::getCreatorName() const {
    return _creator_name;
}

QString Match::getCreatorEmail() const {
    return _creator_email;
}

MatchStatus Match::getStatus() const {
    return _status;
}

int Match::getTopic() const {
    return _topic;
}

int Match::getPlayersCount() const {
    return _player_list.count();
}

const QList<Player> &Match::getPlayerList() const {
    return _player_list;
}

void Match::setId(int id) {
    _id = id;
}

void Match::setStatus(MatchStatus status) {
    _status = status;
}

void Match::setTopic(int topic) {
    _topic = topic;
}

void Match::setPlayerList(const QList<Player> &player_list) {
    _player_list = player_list;
}
