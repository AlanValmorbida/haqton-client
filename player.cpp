/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "player.h"

const QString Player::kKeyId = QString("id");
const QString Player::kKeyName = QString("player_name");
const QString Player::kKeyEmail = QString("player_email");
const QString Player::kKeyApproved = QString("approved");
const QString Player::kKeyScore = QString("score");

Player::Player(const QJsonObject &player_json)
        : _id(-1), _approved(false), _score(0), _answered(false), _last_answer(-1) {
    if (player_json.contains(kKeyId) && player_json[kKeyId].isDouble()) {
        _id = player_json[kKeyId].toInt();
    }
    if (player_json.contains(kKeyName) && player_json[kKeyName].isString()) {
        _name = player_json[kKeyName].toString();
    }
    if (player_json.contains(kKeyEmail) && player_json[kKeyEmail].isString()) {
        _email = player_json[kKeyEmail].toString();
    }
    if (player_json.contains(kKeyApproved) && player_json[kKeyApproved].isBool()) {
        _approved = player_json[kKeyApproved].toBool();
    }
    if (player_json.contains(kKeyScore) && player_json[kKeyScore].isDouble()) {
        _score = player_json[kKeyScore].toInt();
    }
}

Player::~Player() {
}

int Player::getId() const {
    return _id;
}

QString Player::getName() const {
    return _name;
}

QString Player::getEmail() const {
    return _email;
}

bool Player::isApproved() const {
    return _approved;
}

int Player::getScore() const {
    return _score;
}

bool Player::answered() const {
    return _answered;
}

int Player::getLastAnswer() const {
    return _last_answer;
}

void Player::setId(int id) {
    _id = id;
}

void Player::setName(const QString &name) {
    _name = name;
}

void Player::setEmail(const QString &email) {
    _email = email;
}

void Player::setApproval(bool approval) {
    _approved = approval;
}

void Player::setScore(int score) {
    _score = score;
}

void Player::setAnswered(bool answered) {
    _answered = answered;
}

void Player::setLastAnswer(int answer) {
    _last_answer = answer;
}
