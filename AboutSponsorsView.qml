/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    id: aboutHaqtonView
    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: qsTr("Patrocinadores")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 30 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        GridLayout {
            columns: 2
            columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
            Layout.fillWidth: true; Layout.fillHeight: true
            Repeater {
                model: [
                    {
                        image: "assets/icons/qmob.png",
                        text: "<a href='https://qmob.solutions/'>Qmob Solutions</a>"
                    },
                    {
                        image: "assets/icons/opensuse.png",
                        text: "<a href='https://www.opensuse.org/'>openSUSE</a>"
                    },
                    {
                        image: "assets/icons/toradex.png",
                        text: "<a href='https://www.toradex.com/'>Toradex</a>"
                    },
                    {
                        image: "assets/icons/b2open.png",
                        text: "<a href='https://www.b2open.com/'>B2Open Systems</a>"
                    }
                ]
                Item {
                    Layout.fillWidth: true; Layout.fillHeight: true
                    Layout.columnSpan: modelData.text === "<a href='https://qmob.solutions/'>Qmob Solutions</a>" ? 2 : 1
                    Layout.alignment: Qt.AlignCenter
                    ColumnLayout {
                        anchors.fill: parent
                        Image {
                            source: modelData.image
                            fillMode: Image.PreserveAspectFit
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
                        }
                        Label {
                            text: modelData.text
                            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                            horizontalAlignment: Text.AlignHCenter
                            width: aboutHaqtonView.width
                            color: "#1b1b1b"
                            wrapMode: Text.WordWrap
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                            Layout.fillHeight: true
                            onLinkActivated: Qt.openUrlExternally(link)
                        }
                    }
                }
            }
        }

        Button {
            anchors.margins: 20
            text: qsTr("Voltar")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
            onClicked: if (stackView) stackView.pop()
        }

    }
}
