/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "requestscontroller.h"

#include <QJsonDocument>
#include <QNetworkReply>

#include "match.h"

const QString RequestsController::kUrlMatches =
        QStringLiteral("https://haqton.qmob.solutions/matches");
const QString RequestsController::kKeyError = QString("error");
const QString RequestsController::kErrorTokenInvalid = QString("A valid token must be passed.");

RequestsController::RequestsController(const QString &token, QObject *parent)
        : QObject(parent), _token(token), _network_manager(this) {
}

RequestsController::~RequestsController() {
}

void RequestsController::setToken(const QString &token) {
    _token = token;
}

void RequestsController::getMatches() {
    QNetworkRequest request = createRequest(kUrlMatches);

    QNetworkReply *reply = _network_manager.get(request);

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());

        if (!checkErrors(reply_json)) {
            if (reply_json.isArray()) {
                signalMatchListUpdated(reply_json);
            }
        }

        reply->deleteLater();
    });
}

void RequestsController::createMatch(const QString &match_name, const QString &user_name,
                                     const QString &user_email) {
    QNetworkRequest request = createRequest(kUrlMatches);

    QJsonObject json;
    json.insert("description", match_name);
    json.insert("nickname", user_name);
    json.insert("email", user_email);

    QNetworkReply *reply = _network_manager.post(request, QJsonDocument(json).toJson());

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());

        if (!checkErrors(reply_json)) {
            if (reply_json.isObject()) {
                signalNewMatchCreated(reply_json);
            }
        }

        reply->deleteLater();
    });
}

void RequestsController::deleteMatch(int match_id) {
    QNetworkRequest request = createRequest(kUrlMatches + "/" + QString::number(match_id));

    QNetworkReply *reply = _network_manager.deleteResource(request);

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());
        checkErrors(reply_json);

        reply->deleteLater();
    });
}

void RequestsController::enterMatch(int match_id, const QString &user_name,
                                    const QString &user_email) {
    QNetworkRequest request =
            createRequest(kUrlMatches + "/" + QString::number(match_id) + "/players");

    QJsonObject json;
    json.insert("nickname", user_name);
    json.insert("email", user_email);

    QNetworkReply *reply = _network_manager.post(request, QJsonDocument(json).toJson());

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());

        if (!checkErrors(reply_json)) {
            signalMatchEntered(reply_json);
        }

        reply->deleteLater();
    });
}

void RequestsController::updateMatchStatus(int match_id, int status) {
    QNetworkRequest request = createRequest(kUrlMatches + "/" + QString::number(match_id));

    QJsonObject json;
    json.insert("status", status);

    QNetworkReply *reply = _network_manager.put(request, QJsonDocument(json).toJson());

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());

        if (!checkErrors(reply_json)) {
            signalMatchUpdated(reply_json);
        }

        reply->deleteLater();
    });
}

void RequestsController::updatePlayer(int match_id, int player_id, bool approved) {
    QNetworkRequest request = createRequest(kUrlMatches + "/" + QString::number(match_id) +
                                            "/players/" + QString::number((player_id)));

    QJsonObject json;
    json.insert("approved", approved);
    json.insert("score", 0);

    QNetworkReply *reply = _network_manager.put(request, QJsonDocument(json).toJson());

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());

        if (!checkErrors(reply_json)) {
            signalPlayersUpdated(reply_json);
        }

        reply->deleteLater();
    });
}

void RequestsController::getRandomQuestion(int match_id) {
    QNetworkRequest request =
            createRequest(kUrlMatches + "/" + QString::number(match_id) + "/random_question");

    QNetworkReply *reply = _network_manager.get(request);

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());
        checkErrors(reply_json);

        reply->deleteLater();
    });
}

void RequestsController::processAnswer(int match_id, int player_id, int question_id,
                                       int answer_id) {
    QNetworkRequest request = createRequest(kUrlMatches + "/" + QString::number(match_id) +
                                            "/players/" + QString::number(player_id) + "/answer");

    QJsonObject json;
    json.insert("question_id", question_id);
    json.insert("player_option", answer_id);

    QNetworkReply *reply = _network_manager.post(request, QJsonDocument(json).toJson());

    connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        QJsonDocument reply_json = QJsonDocument::fromJson(reply->readAll());
        checkErrors(reply_json);

        reply->deleteLater();
    });
}

QNetworkRequest RequestsController::createRequest(const QString &url) {
    QNetworkRequest request(QUrl({url}));
    request.setRawHeader(QByteArray("Authorization"),
                         QByteArray(("Bearer " + _token).toStdString().c_str()));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/json"));
    return request;
}

bool RequestsController::checkErrors(const QJsonDocument &json) {
    if (json.isObject()) {
        QJsonObject json_obj = json.object();
        if (json_obj.contains(kKeyError)) {
            if (json_obj[kKeyError].toString() == kErrorTokenInvalid) {
                signalInvalidToken();
            }

            return true;
        }
    }

    return false;
}
