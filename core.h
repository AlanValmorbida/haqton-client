/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CORE_H_
#define CORE_H_

#include <QObject>

class AuthenticationController;
class ConfigurationController;
class GameController;
class MatchesListModel;
class MessagingController;
class PlayersListModel;
class QuestionListModel;
class UserController;

class Core : public QObject {
    Q_OBJECT
    Q_PROPERTY(MessagingController *messagingController READ messagingController CONSTANT)
    Q_PROPERTY(AuthenticationController *authenticationController READ authenticationController
                       CONSTANT)
    Q_PROPERTY(
            ConfigurationController *configurationController READ configurationController CONSTANT)
    Q_PROPERTY(GameController *gameController READ gameController CONSTANT)
    Q_PROPERTY(UserController *userController READ userController CONSTANT)
    Q_PROPERTY(MatchesListModel *matchesListModel READ matchesListModel CONSTANT)
    Q_PROPERTY(PlayersListModel *playersListModel READ playersListModel CONSTANT)
    Q_PROPERTY(QuestionListModel *questionListModel READ questionListModel CONSTANT)

 public:
    ~Core() Q_DECL_OVERRIDE;

    //! Get the Core singleton.
    /*!
      \return The Core singleton
    */
    static Core *instance();

    //! Get the MessagingController instance.
    /*!
      \return The MessagingController instance
    */
    MessagingController *messagingController() const;

    //! Get the AuthenticationController instance.
    /*!
      \return The AuthenticationController instance
    */
    AuthenticationController *authenticationController() const;

    //! Get the ConfigurationController instance.
    /*!
      \return The ConfigurationController instance
    */
    ConfigurationController *configurationController() const;

    //! Get the GameController instance.
    /*!
      \return The GameController instance
    */
    GameController *gameController() const;

    //! Get the UserController instance.
    /*!
      \return The UserController instance
    */
    UserController *userController() const;

    //! Get the MatchesListModel instance.
    /*!
      \return The MatchesListModel instance
    */
    MatchesListModel *matchesListModel() const;

    //! Get the PlayersListModel instance.
    /*!
      \return The PlayersListModel instance
    */
    PlayersListModel *playersListModel() const;

    //! Get the QuestionListModel instance.
    /*!
      \return The QuestionListModel instance
    */
    QuestionListModel *questionListModel() const;

 private:
    explicit Core(QObject *parent = nullptr);

    static Core *_instance;
    MessagingController *_messaging_controller;
    AuthenticationController *_authentication_controller;
    ConfigurationController *_configuration_controller;
    GameController *_game_controller;
    UserController *_user_controller;
    MatchesListModel *_matches_list_model;
    PlayersListModel *_players_list_model;
    QuestionListModel *_question_list_model;
};

#endif  // CORE_H_
