/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 1.4 as Quick1
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import solutions.qmob.haqton 1.0

import "FontAwesome"

Item {
    id: questionView
    property bool creator: false
    property bool already_answered: false

    width: stackView.width
    height: stackView.height

    ColumnLayout {
        spacing: internal.margins
        anchors.fill: parent

        Label {
            text: Core.questionListModel.description
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 26 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: true
        }

        Quick1.TableView {
            id: tvQuestion
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            verticalScrollBarPolicy: Qt.ScrollBarAsNeeded
            frameVisible: false
            backgroundVisible: false
            selectionMode: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: 100
            model: Core.questionListModel
            headerVisible: false
            rowDelegate: Component {
                Button {
                    property bool selected: false
                    property bool right_option: Core.questionListModel.getRightOption() === styleData.row
                    height: 35
                    bottomInset: 1
                    topInset: 1
                    background: Rectangle {
                        color: parent.right_option && questionView.already_answered ? "green" : "#AAAAAA"
                        opacity: 0.6
                        radius: 2
                        width: tvQuestion.width
                        border.color: "black"
                        border.width: parent.selected ? 3 : 0
                    }
                    onClicked: {
                        if (!questionView.already_answered) {
                            questionView.already_answered = true
                            Core.gameController.processAnswer(styleData.row)
                            selected = true
                        }
                    }
                }
            }

            Quick1.TableViewColumn {
                role: "option"
                title: ""
                width: 25
                delegate: Label {
                    text: styleData.value
                    color: "black"
                    anchors.fill: parent
                    anchors.margins: 5
                    verticalAlignment: Qt.AlignVCenter
                }
            }
            Quick1.TableViewColumn {
                role: "description"
                width: stackView.width - 25
                title: qsTr("Alternativa")
                delegate: Label {
                    text: styleData.value
                    color: "black"
                    anchors.fill: parent
                    verticalAlignment: Qt.AlignVCenter
                }
            }
        }

        Quick1.TableView {
            id: tvPlayers
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            verticalScrollBarPolicy: Qt.ScrollBarAsNeeded
            frameVisible: false
            backgroundVisible: false
            selectionMode: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            model: Core.playersListModel
            Quick1.TableViewColumn {
                role: "name"
                title: qsTr("Participante")
                width: stackView.width / 3
                delegate: Label {
                    text: styleData.value
                }
            }
            Quick1.TableViewColumn {
                id: tvcAnswer
                role: "answer"
                title: qsTr("Resposta")
                width: stackView.width / 3
                visible: false
                delegate: Label {
                    text: styleData.value
                    horizontalAlignment: Qt.AlignHCenter
                }
            }
            Quick1.TableViewColumn {
                id: tvcAnswered
                role: "answered"
                title: "respondeu"
                width: stackView.width / 3
                visible: true
                delegate: Item {
                    anchors.fill: parent
                    RowLayout {
                        anchors.fill: parent
                        Label {
                            text: Icons.faHourglassHalf
                            visible: !styleData.value
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            Layout.alignment: Qt.AlignCenter
                            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 16 }
                            color: "yellow"
                            ToolTip.text: qsTr("Ainda não respondeu")
                            ToolTip.visible: maWaiting.containsMouse
                            MouseArea {
                                id: maWaiting
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                        }
                        Label {
                            text: Icons.faCheck
                            visible: styleData.value
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            Layout.alignment: Qt.AlignCenter
                            font { family: FontAwesome.solid; styleName: "Solid"; pixelSize: 18 }
                            color: "green"
                            ToolTip.text: qsTr("Já respondeu")
                            ToolTip.visible: maApproved.containsMouse
                            MouseArea {
                                id: maApproved
                                anchors.fill: parent
                                hoverEnabled: true
                            }
                        }
                    }
                }
            }
            Quick1.TableViewColumn {
                role: "score"
                title: qsTr("Pontuação")
                width: stackView.width / 3
                delegate: Label {
                    text: styleData.value
                    horizontalAlignment: Qt.AlignRight
                }
            }
            style: TableViewStyle {
                textColor: "black"
                alternateBackgroundColor: "#80AAAAAA"
            }
        }

        Label {
            id: lblMatchFinished
            text: qsTr("Partida finalizada")
            font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 24 }
            horizontalAlignment: Text.AlignHCenter
            color: "#1b1b1b"
            visible: false
            wrapMode: Text.WordWrap
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
        }

        RowLayout {
            spacing: internal.margins / 2
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            Layout.fillWidth: true
            Button {
                id: btFinishMatch
                text: qsTr("Encerrar Partida")
                visible: questionView.creator
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    messageDialogFinishMatch.buttons = MessageDialog.Cancel | MessageDialog.Ok
                    messageDialogFinishMatch.open()
                }
            }
            Button {
                id: btExitMatch
                text: qsTr("Sair")
                visible: !questionView.creator
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    messageDialogExit.buttons = MessageDialog.Cancel | MessageDialog.Ok
                    messageDialogExit.open()
                }
            }
            Button {
                id: btNextQuestion
                text: qsTr("Próxima Pergunta")
                visible: false
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    if (questionView.state == "") {
                        if (questionView.creator) {
                            Core.gameController.getRandomQuestion()
                        }
                    } else {
                        Core.questionListModel.startTableUpdate()
                        Core.playersListModel.startTableUpdate()
                        questionView.state = ""
                        questionView.already_answered = false
                    }
                }
            }
            Button {
                id: btFinalResult
                text: qsTr("Ver Resultado Final")
                visible: false
                Layout.fillWidth: true
                Layout.preferredWidth: 100
                onClicked: {
                    Core.questionListModel.startTableUpdate()
                    Core.playersListModel.startTableUpdate()
                    stackView.push("qrc:/MatchFinishedView.qml", {})
                }
            }
        }
    }

    MessageDialog {
        id: messageDialogExit
        buttons: MessageDialog.Cancel | MessageDialog.Ok
        text: qsTr("Você realmente deseja sair da partida?\nEsta ação não poderá ser desfeita.")
        title: qsTr("Sair")
        onOkClicked: {
            Core.gameController.exitCurrentMatch()
            Core.questionListModel.startTableUpdate()
            Core.playersListModel.startTableUpdate()
            if (stackView) {
                stackView.pop()
                stackView.pop()
            }
        }
    }

    MessageDialog {
        id: messageDialogFinishMatch
        buttons: MessageDialog.Cancel | MessageDialog.Ok
        text: qsTr("Você realmente deseja finalizar a partida?\nEsta ação não poderá ser desfeita.")
        title: qsTr("Finalizar")
        onOkClicked: {
            Core.gameController.finishCurrentMatch()
        }
    }

    states: [
        State {
            name: "showAnswers"
            PropertyChanges {
                target: tvQuestion
                Layout.preferredHeight: 50
            }
            PropertyChanges {
                target: tvPlayers
                Layout.preferredHeight: 100
            }
            PropertyChanges {
                target: tvcAnswer
                visible: true
            }
            PropertyChanges {
                target: tvcAnswered
                visible: false
            }
            PropertyChanges {
                target: btNextQuestion
                visible: true
            }
        },
        State {
            name: "matchFinished"
            PropertyChanges {
                target: tvQuestion
                Layout.preferredHeight: 50
            }
            PropertyChanges {
                target: tvPlayers
                Layout.preferredHeight: 100
            }
            PropertyChanges {
                target: tvcAnswer
                visible: true
            }
            PropertyChanges {
                target: tvcAnswered
                visible: false
            }
            PropertyChanges {
                target: btFinishMatch
                visible: false
            }
            PropertyChanges {
                target: btExitMatch
                visible: false
            }
            PropertyChanges {
                target: btNextQuestion
                visible: false
            }
            PropertyChanges {
                target: btFinalResult
                visible: true
            }
            PropertyChanges {
                target: lblMatchFinished
                visible: true
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                easing.type: Easing.OutQuad
                properties: "Layout.preferredHeight"
                duration: 1000
            }
        }
    ]

    Component.onCompleted: {
        Core.questionListModel.startTableUpdate()
        Core.playersListModel.startTableUpdate()
    }

    Connections {
        target: Core.gameController
        enabled: true
        function onSignalPlayerRejected() {
            if (stackView) {
                stackView.pop()
                stackView.pop()
            }
        }
        function onSignalNewQuestion() {
            Core.questionListModel.stopTableUpdate()
            Core.playersListModel.stopTableUpdate()
            questionView.state = "showAnswers"
        }
        function onSignalMatchFinished() {
            questionView.state = "matchFinished"
        }
    }
}
