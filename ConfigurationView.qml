/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import solutions.qmob.haqton 1.0

import "FontAwesome"

Page {
    id: configurationView
    property int initialTab: 0

    background: Rectangle {
        color: "#666666"
        opacity: 0.5
    }

    header: TabBar {
        id: bar
        width: parent.width
        TabButton {
            text: qsTr("Usuário")
        }
        TabButton {
            text: qsTr("Autenticação")
        }
        TabButton {
            text: qsTr("Volume")
        }
    }

    MessageDialog {
        id: messageDialog
        buttons: MessageDialog.Ok
    }

    FileDialog {
        id: fileDialog
        nameFilters: [qsTr("JWT Token (*.jwt)")]
        file: "file://" + Core.authenticationController.getTokenFile()
    }

    SwipeView {
        id: configSwipeView
        anchors.fill: parent
        width: parent.width
        currentIndex: bar.currentIndex
        Item {
            id: userTab
            visible: bar.currentIndex == 0
            width: configSwipeView.width
            ColumnLayout {
                spacing: internal.margins
                anchors.centerIn: parent
                width: parent.width
                RowLayout {
                    spacing: internal.margins
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    Item{
                        Layout.fillWidth: true
                    }
                    Label {
                        id: lblUserName
                        text: qsTr("Nome:")
                        font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                        horizontalAlignment: Text.AlignRight
                        color: "#1b1b1b"
                        wrapMode: Text.WordWrap
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }
                    TextField {
                        id: tfUserName
                        text: Core.userController.getUserName()
                        font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 18 }
                        selectByMouse: true
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300
                    }
                    Item{
                        Layout.fillWidth: true
                    }
                }

                RowLayout {
                    spacing: internal.margins
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    Item{
                        Layout.fillWidth: true
                    }
                    Label {
                        id: lblUserEmail
                        text: qsTr("Email:")
                        font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                        horizontalAlignment: Text.AlignRight
                        color: "#1b1b1b"
                        wrapMode: Text.WordWrap
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }
                    TextField {
                        id: tfUserEmail
                        text: Core.userController.getUserEmail()
                        font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 18 }
                        selectByMouse: true
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        Layout.fillWidth: true
                        Layout.maximumWidth: 300
                    }
                    Item{
                        Layout.fillWidth: true
                    }
                }
            }
        }
        Item {
            id: authenticationTab
            visible: bar.currentIndex == 1
            width: configurationView.width
            RowLayout {
                anchors.centerIn: parent
                width: authenticationTab.width
                spacing: internal.margins
                Item{
                    Layout.fillWidth: true
                }
                Label {
                    id: lblTokenFile
                    text: qsTr("Token:")
                    font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                    horizontalAlignment: Text.AlignRight
                    color: "#1b1b1b"
                    wrapMode: Text.WordWrap
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                }
                TextField {
                    id: tfTokenFile
                    text: fileDialog.file
                    enabled: false
                    font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 16 }
                    selectByMouse: true
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.maximumWidth: 500
                }
                Button {
                     id: btTokenFile
                     text: qsTr("Abrir")
                     onClicked: fileDialog.open()
                }
                Item{
                    Layout.fillWidth: true
                }
            }
        }
        Item {
            id: volumeTab
            visible: bar.currentIndex == 2
            width: configurationView.width
            RowLayout {
                anchors.centerIn: parent
                width: volumeTab.width
                spacing: internal.margins
                Item{
                    Layout.fillWidth: true
                }
                Label {
                    id: lblVolume
                    text: qsTr("Volume:")
                    font { family: FontAwesome.regular; styleName: "Regular"; pixelSize: 20 }
                    horizontalAlignment: Text.AlignRight
                    color: "#1b1b1b"
                    wrapMode: Text.WordWrap
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                }
                Slider {
                    id: sliderVolume
                    from: 1
                    value: Core.configurationController.volume * 100
                    to: 100
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.maximumWidth: 500
                }
                Item{
                    Layout.fillWidth: true
                }
            }
        }
        onCurrentIndexChanged: {
            bar.currentIndex = configSwipeView.currentIndex
        }
    }

    footer: RowLayout {
        spacing: internal.margins / 2
        Button {
            text: qsTr("Voltar")
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Layout.leftMargin: internal.margins / 2
            onClicked: {
                if (Core.userController.isUserValid()) {
                    if (stackView) {
                        stackView.pop()
                    }
                } else {
                    messageDialog.title = qsTr("Usuário inválido")
                    messageDialog.text = qsTr("Por favor, insira e salve um usuário válido.")
                    messageDialog.open()
                }

            }
        }
        Button {
            text: qsTr("Salvar")
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Layout.rightMargin: internal.margins/2
            onClicked: {
                if (tfUserName.text.length === 0) {
                    messageDialog.title = qsTr("Nome inválido")
                    messageDialog.text = qsTr("Por favor, insira um nome válido.")
                    messageDialog.open()
                } else if (tfUserEmail.text.length === 0){
                    messageDialog.title = qsTr("E-mail inválido")
                    messageDialog.text = qsTr("Por favor, insira um e-mail válido.")
                    messageDialog.open()
                } else {
                    if (tfUserName.text !== Core.userController.getUserName()) {
                        Core.userController.setUserName(tfUserName.text)
                    }
                    if (tfUserEmail.text !== Core.userController.getUserEmail()) {
                        Core.userController.setUserEmail(tfUserEmail.text)
                    }
                    if (sliderVolume.value / 100 !== Core.configurationController.volume) {
                        Core.configurationController.setVolume(sliderVolume.value / 100)
                    }
                    var tokenFile = fileDialog.file.toString().replace("file://", "")
                    if (tokenFile !== Core.authenticationController.getTokenFile()) {
                        Core.authenticationController.setTokenFile(tokenFile)
                    }

                    if (stackView) {
                        stackView.pop()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        bar.currentIndex = initialTab
    }
}
